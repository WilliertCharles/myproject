package aspn.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import aspn.Domain.Calculator;
import aspn.Exception.CalException;
import aspn.Service.CalculatorServiceImpl;

@Controller
public class CalculatorController {
 
  @Autowired
  CalculatorServiceImpl calculatorService;
  
  @RequestMapping(value={"/","/Calculator"} , method = RequestMethod.GET)
  public String inputCalc(Calculator calculator ) throws CalException {
   	  
       return  "CalculatorForm" ;    
   }
   
  @RequestMapping(value= "/Calculator" , method = RequestMethod.POST)
  public String handleCalc(Calculator calculator) throws CalException {
  
	  
	  
 		   if(calculator.getAdd1()==null||calculator.getAdd2()==null)
 			throw new CalException("Something wrong !");
 			calculatorService.add(calculator);
 		
 			
		     
 
		if (calculator.getMult1() != null &&
				 calculator.getMult2() != null)
			calculatorService.mult(calculator);
			
 	 return  "CalculatorView" ;
   }
   
 
}
