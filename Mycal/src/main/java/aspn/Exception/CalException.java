package aspn.Exception;



@SuppressWarnings("serial")
public class CalException extends Exception {
	public CalException() {
		super();
	}
	public CalException(String msg) {
		super(msg);
	}
	public CalException(Throwable t) { 
		super(t);
	}
}
