package aspn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MycalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MycalApplication.class, args);
	}

}
